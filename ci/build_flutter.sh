#!/bin/bash
set -e

# compile aot
./flutterw build aot

# print coverage
./flutterw test --coverage
genhtml -o coverage/html coverage/lcov.info

# linting
./flutterw analyze

# format with line width 120
./.flutter/bin/cache/dart-sdk/bin/dartfmt --set-exit-if-changed -w -l 120 lib test